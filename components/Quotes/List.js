import React, {Component} from 'react';
import {ScrollView, TouchableOpacity, Text, Button, RefreshControl, Linking, StyleSheet} from "react-native";
import QuotesListItem from './ListItem';
import Loader from '../Loader';

class QuotesList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false
    }
  }

  onRefresh = () => {
    this.setState({refreshing: true});
    this.fetchQuote();
    this.setState({refreshing: false});
  };

  onQuotePress = quote => {
    this.props.onQuotePress(quote);
  };

  renderQuotesItem = () => {
    return this.props.quotesList.map((item, index) => {
      return <QuotesListItem key={index}
                             quote={item}
                             onPress={this.onQuotePress}
                             onLongPress={quote => Linking.openURL(quote._embedded.source[0].url)}
      />
    })
  };

  fetchQuote = () => {
    this.props.actions.fetchQuote();
  };

  render() {
    if (this.props.isLoaderDisplayed) {
      return <Loader/>
    }
    return (
      <ScrollView style={styles.wrapper}
                  contentContainerStyle={{flexDirection: 'column', alignItems: 'center'}}
                  refreshControl={<RefreshControl refreshing={this.state.refreshing}
                                                  onRefresh={this.onRefresh}
                  />}
      >
        <Text style={styles.title}>Donald Trump a dit...</Text>
        <TouchableOpacity style={styles.buttonContainer}>
          <Button title={'Avoir une citation'}
                  onPress={this.fetchQuote}
                  color='#1abc9c'
          />
        </TouchableOpacity>
        {this.renderQuotesItem()}
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: '#1abc9c',
    paddingTop: 24
  },
  buttonContainer: {
    flex: 0.5,
    justifyContent: 'center',
    backgroundColor: '#fff',
    width: '50%',
    borderRadius: 5,
    marginTop: 24
  },
  title: {
    fontWeight: '300',
    fontSize: 30,
    color: '#fff'
  }
});

export default QuotesList;
