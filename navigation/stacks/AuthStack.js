import React from 'react';
import {createStackNavigator} from "react-navigation";
import AuthLoadingScreen from '../../screens/AuthLoadingScreen';
import LoginScreen from '../../screens/LoginScreen';

const AuthStack = createStackNavigator({
    AuthLoading: AuthLoadingScreen,
    Auth: LoginScreen
  }, {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#c0392b',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontSize: 20,
      },
    },
  }
);

export default AuthStack;
