import React from 'react';
import {createStackNavigator} from 'react-navigation';
import QuotesListScreen from "../../screens/QuotesListScreen";
import QuoteDetailsScreen from '../../screens/QuoteDetailsScreen';

const QuotesStack = createStackNavigator({
    QuotesList: QuotesListScreen,
    QuoteDetails: QuoteDetailsScreen
  },
  {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#16a085',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontSize: 20,
        alignSelf: 'center'
      },
    },
  });
QuotesStack.navigationOptions = {
  tabBarLabel: 'Quotes'
};
export default QuotesStack;
