import React from 'react';
import {createStackNavigator} from "react-navigation";
import HomeScreen from '../../screens/HomeScreen';

const HomeStack = createStackNavigator({
    Home: HomeScreen,
  }, {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#8e44ad',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontSize: 20,
      },
    },
  }
);

HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
};

export default HomeStack;
