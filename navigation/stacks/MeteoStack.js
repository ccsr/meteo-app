import React from 'react';
import {createStackNavigator} from 'react-navigation';
import MeteoScreen from '../../screens/MeteoScreen';

const MeteoStack = createStackNavigator({
    Meteo: MeteoScreen,
  },
  {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#2980b9',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontSize: 20,
      },
    },
  }
);
MeteoStack.navigationOptions = {
  tabBarLabel: 'Meteo'
};
export default MeteoStack;
