import React from 'react';
import {Alert, AsyncStorage} from 'react-native';
import {withNavigationFocus} from 'react-navigation'

const AuthWrapper = route => {
    return Screen => {
      return withNavigationFocus(class extends React.Component {

          componentDidMount() {
            this.redirectToLoginScreen();
          }

          redirectToLoginScreen = async () => {
            const token = await AsyncStorage.getItem('userToken');
            const response = await fetch(`https://graph.facebook.com/me?access_token=${token}`);
            const data = await response.json();
            if (data.ok === false) {
              this.props.navigation.navigate(route)
            }
          };

          render() {
            if(this.props.isFocused)
              this.redirectToLoginScreen();
            return <Screen {...this.props} />
          }
        }
      )
    }
  }
;

export default AuthWrapper;

// authWrapper('Auth')(Component);
