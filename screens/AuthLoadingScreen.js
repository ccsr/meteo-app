import React, {Component} from 'react';
import {View} from 'react-native';
import {ActivityIndicator, AsyncStorage, StatusBar} from 'react-native'

class AuthLoadingScreen extends Component {
  constructor(props) {
    super(props);
    this.checkIfUserIsLogged();
  }

  static navigationOptions = {
    title: 'Authentification',
  };

  checkIfUserIsLogged = async () => {
    const userToken = await AsyncStorage.getItem('userToken');
    this.props.navigation.navigate(userToken ? 'Home' : 'Auth')
  };

  render() {
    return (
      <View>
        <ActivityIndicator/>
        <StatusBar barStyle="default"/>
      </View>

    )
  }
}

export default AuthLoadingScreen;
