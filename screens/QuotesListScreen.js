import React, {Component} from 'react';
import QuotesList from "../containers/QuotesList";
import AuthWrapper from "../hoc/auth";

class QuotesListScreen extends Component {
  static navigationOptions = {
    title: 'Quotes List',
  };

  handleNavigation = quote => {
    this.props.navigation.navigate('QuoteDetails', {quote})
  };

  render() {
    return (
      <QuotesList onQuotePress={this.handleNavigation}/>
    )
  }
}

export default AuthWrapper('Auth')(QuotesListScreen);
