import React, {Component} from 'react';
import {Button, View, Alert, TouchableOpacity, StyleSheet, AsyncStorage} from 'react-native';
import {Facebook} from 'expo';

class LoginScreen extends Component {
  static navigationOptions = {
    title: 'Authentification',
  };

  bypassLogIn = async () => {
    await AsyncStorage.setItem('userToken', 'bypass');
    this.props.navigation.navigate('Home');
  };

  logIn = async () => {
    try {
      const {
        type,
        token,
        expires,
        permissions,
        declinedPermissions,
      } = await Facebook.logInWithReadPermissionsAsync('1121386381376964', {
        permissions: ['public_profile'],
      });
      if (type === 'success') {
        // Get the user's name using Facebook's Graph API
        const response = await fetch(`https://graph.facebook.com/me?access_token=${token}`);
        Alert.alert('Connecté.e !', `Bonjour ${(await response.json()).name}!`);
        await AsyncStorage.setItem('userToken', token);
        this.props.navigation.navigate('Home');
      } else {
        // type === 'cancel'
      }
    } catch ({message}) {
      alert(`Facebook Login Error: ${message}`);
    }
  };

  render() {
    return (
      <View style={styles.wrapper}>
        <TouchableOpacity style={styles.buttonContainer}
                          onPress={this.logIn}
        >
          <Button accessibilityLabel={"Se connecter via Facebook"}
                  title={'Se connecter via Facebook'}
                  color={'#fff'}
                  onPress={this.logIn}
          />
        </TouchableOpacity>
        <Button onPress={() => this.bypassLogIn()}
                title={'Se connecter simplement'}
                color={'orange'}
                accessibilityLabel={'Se connecter simplement'}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#e74c3c'
  },
  buttonContainer: {
    borderRadius: 5,
    borderColor: '#fff',
    borderWidth: 1,
    padding: 10,
    marginBottom: 30
  }
});

export default LoginScreen;
