import React from 'react';
import {View, Text, AsyncStorage, TouchableOpacity, Button, StyleSheet} from 'react-native';
import AuthWrapper from "../hoc/auth";

class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Home',
  };

  logOut = async () => {
    await AsyncStorage.removeItem('userToken');
    this.props.navigation.navigate('Auth');
  };

  render() {
    return (
      <View style={styles.wrapper}>
        <Text style={styles.text}>Bienvenue sur Meteo Forecast !</Text>
        <TouchableOpacity style={{padding: 10, borderColor: '#fff', borderWidth: 1, borderRadius: 5, marginTop: 30}}
                          onPress={this.logOut}
        >
          <Button onPress={this.logOut}
                  title='Se déconnecter'
                  color='#fff'
                  accessibilityLabel='Se déconnecter'
          />
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#9b59b6'
  },
  text: {
    fontSize: 48,
    fontWeight: '300',
    color: '#fff'
  }
});

export default AuthWrapper('Auth')(HomeScreen);
