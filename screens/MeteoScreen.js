import React, {Component} from 'react';
import Title from "../components/Title";
import Forecast from '../containers/Forecast';
import {ScrollView, Text, StyleSheet} from "react-native";
import {Constants, Location, Permissions} from 'expo';
import AuthWrapper from "../hoc/auth";

class MeteoScreen extends Component {
  state = {
    backgroundColor: '#3E8BB8',
    location: null,
    errorMessage: null
  };

  static navigationOptions = {
    title: 'Meteo',
  };

  componentDidMount() {
    this.getLocationAsync();
  }

  setBackgroundColor = weather => {
    if (weather === 1000) {
      this.setState({backgroundColor: '#f1c40f'});
    } else if (weather === 1003) {
      this.setState({backgroundColor: '#95a5a6'});
    } else if (weather === 1189) {
      this.setState({backgroundColor: '#34495e'});
    }
  };

  getLocationAsync = async () => {
    let {status} = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        errorMessage: 'Permission to access location was denied',
      });
    }

    let location = await Location.getCurrentPositionAsync({});
    const latitudeLongitude = `${location.coords.latitude},${location.coords.longitude}`;
    this.setState({location: latitudeLongitude});
  };

  render() {
    let text = 'Waiting..';
    if (this.state.errorMessage) {
      text = this.state.errorMessage;
    } else if (this.state.location) {
      text = JSON.stringify(this.state.location);
    }

    return (
      <ScrollView style={[styles.globalContainer, {backgroundColor: this.state.backgroundColor}]}>
        <Title title='Meteo Forecast'/>
        <Forecast setBackground={this.setBackgroundColor} location={this.state.location}/>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  globalContainer: {
    flex: 1
  }
});

export default AuthWrapper('Auth')(MeteoScreen);
